<?php

require_once("Animal.php");
require_once("Ape.php");
require_once("Frog.php");

$sheep = new Animal("Shaun");
echo "Name =  ".$sheep->name ."<br>";
echo "Legs =  ".$sheep->legs ."<br>";
echo "Cold Bloded =  ".$sheep->cold_blooded ."<br> <br>";

$kodok = new Frog ("Buduk");
echo "Name =  ".$kodok->name ."<br>";
echo "Legs =  ".$kodok->legs ."<br>";
echo "Cold Bloded =  ".$kodok->cold_blooded ."<br>";
echo "Yell = " .$kodok->jump ."<br> <br>" ;

$sungokong = new Ape("Kera Sakti");
echo "Name =  ".$sungokong->name ."<br>";
echo "Legs =  ".$sungokong->legs ."<br>";
echo "Cold Bloded =  ".$sungokong->cold_blooded ."<br>";
echo "Yell = " .$sungokong->yell ."<br>";
?>
